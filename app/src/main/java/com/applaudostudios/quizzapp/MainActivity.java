package com.applaudostudios.quizzapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.applaudostudios.quizzapp.adapters.QuestionsAdapter;
import com.applaudostudios.quizzapp.interfaces.OnItemListener;
import com.applaudostudios.quizzapp.model.Question;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnItemListener {

    private List<Question> questiond;
    private RecyclerView rcvQuestions;
    private QuestionsAdapter questionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcvQuestions = findViewById(R.id.rcvQuestions);
        rcvQuestions.setLayoutManager(new LinearLayoutManager(this));
        questionsAdapter = new QuestionsAdapter(this);
        questionsAdapter.setOnItemListener(this);
        rcvQuestions.setAdapter(questionsAdapter);
        questionsAdapter.setData(Question.loadQuestions(this));
    }

    @Override
    public void onClick(Question question) {

    }
}
