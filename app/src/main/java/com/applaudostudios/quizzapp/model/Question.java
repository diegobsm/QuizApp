package com.applaudostudios.quizzapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.applaudostudios.quizzapp.R;

import java.util.ArrayList;
import java.util.List;

public class Question implements Parcelable {

    private String question;
    private List<Options> options;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Options> getOptions() {
        return options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    public static List<Question> loadQuestions(Context c){
        String [] questions = c.getResources().getStringArray(R.array.questions);
        List<Question> allQuestions = new ArrayList<>();
        for (int i=0;i<questions.length;i++){
            Question question = new Question();
            question.question = questions[i];
            question.options = Options.getOptionsByQuestion(c,i);
            allQuestions.add(question);
        }
        return allQuestions;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.question);
        dest.writeTypedList(this.options);
    }

    public Question() {
    }

    protected Question(Parcel in) {
        this.question = in.readString();
        this.options = in.createTypedArrayList(Options.CREATOR);
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
