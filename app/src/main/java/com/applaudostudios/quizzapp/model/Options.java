package com.applaudostudios.quizzapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.applaudostudios.quizzapp.R;

import java.util.ArrayList;
import java.util.List;

public class Options implements Parcelable {

    private String options;
    private boolean answer;

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }


    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        answer = answer;
    }

    protected static List<Options> getOptionsByQuestion(Context c, int numQuestion) {
        String[] optionsQuestion;
        String[] answersByQuestion;
        switch (numQuestion) {
            case 0:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_one);
                answersByQuestion = c.getResources().getStringArray(R.array.answers_question_one);
                break;
            case 1:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_two);
                answersByQuestion = c.getResources().getStringArray(R.array.answers_question_two);
                break;
            case 2:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_three);
                answersByQuestion = c.getResources().getStringArray(R.array.options_question_three);
                break;
            case 3:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_four);
                answersByQuestion = c.getResources().getStringArray(R.array.answer_question_four);
                break;
            case 4:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_five);
                answersByQuestion = c.getResources().getStringArray(R.array.answer_question_five);
                break;
            case 5:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_six);
                answersByQuestion = c.getResources().getStringArray(R.array.answer_question_six);
                break;
            case 6:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_seven);
                answersByQuestion = c.getResources().getStringArray(R.array.answer_question_seven);
                break;
            case 7:
                optionsQuestion = c.getResources().getStringArray(R.array.options_question_eight);
                answersByQuestion = c.getResources().getStringArray(R.array.answer_question_eight);
                break;
            default:
                optionsQuestion = new String[0];
                answersByQuestion = new String[0];
                break;
        }
        List<Options> options = new ArrayList<>();
        for (int i = 0; i < optionsQuestion.length; i++) {
            Options opti = new Options();
            boolean isCorrect = false;
            for (String s : answersByQuestion) {
                if (i == Integer.parseInt(s)) {
                    isCorrect = true;
                    break;
                }
            }
            opti.setAnswer(isCorrect);
            opti.setOptions(optionsQuestion[i]);
            options.add(opti);
        }
        return options;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.options);
        dest.writeByte(this.answer ? (byte) 1 : (byte) 0);
    }

    public Options() {
    }

    protected Options(Parcel in) {
        this.options = in.readString();
        this.answer = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Options> CREATOR = new Parcelable.Creator<Options>() {
        @Override
        public Options createFromParcel(Parcel source) {
            return new Options(source);
        }

        @Override
        public Options[] newArray(int size) {
            return new Options[size];
        }
    };
}
