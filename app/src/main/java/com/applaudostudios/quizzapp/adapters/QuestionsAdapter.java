package com.applaudostudios.quizzapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applaudostudios.quizzapp.R;
import com.applaudostudios.quizzapp.interfaces.OnItemListener;
import com.applaudostudios.quizzapp.model.Question;

import java.util.ArrayList;
import java.util.List;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionsViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Question> questions;



    private OnItemListener onItemListener;

    public QuestionsAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        questions = new ArrayList<>();
    }

    public void setData(List<Question> data){
        questions = data;
        notifyDataSetChanged();
    }

    public void setOnItemListener(OnItemListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    @NonNull
    @Override
    public QuestionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_question,parent,false);
        return new QuestionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionsViewHolder holder, int position) {
        holder.txvQuestion.setText(questions.get(position).getQuestion());
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public class QuestionsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txvQuestion;
        CardView lyMain;

        public QuestionsViewHolder(View itemView) {
            super(itemView);
            txvQuestion = itemView.findViewById(R.id.txvQuestion);
            lyMain = itemView.findViewById(R.id.lyMain);
            lyMain.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemListener.onClick(questions.get(getAdapterPosition()));
        }
    }
}
