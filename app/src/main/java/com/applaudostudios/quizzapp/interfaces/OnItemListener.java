package com.applaudostudios.quizzapp.interfaces;

import com.applaudostudios.quizzapp.model.Question;

public interface OnItemListener {
    void onClick(Question question);
}
